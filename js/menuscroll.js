
var nav = document.querySelector('nav');
document.addEventListener('scroll', showBg);
function showBg() {
  if(window.pageYOffset>100){
    nav.classList.add('navbar-bg-scroll');
  }
  else {
      nav.classList.remove('navbar-bg-scroll');
  }
}
