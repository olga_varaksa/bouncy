
var isCirclesInited = false;
var isSlide1Inited = false;
window.onscroll = function() {
  var scrolled = window.pageYOffset || document.documentElement.scrollTop;
  if ((scrolled >= 2500) && !isCirclesInited){
    isCirclesInited = true;
    var circleBranding = Circles.create({
      id:                  'circle-branding',
      radius:              70,
      value:               80,
      maxValue:            100,
      width:               3,
      text:                function(value){return value + '%';},
      colors:              ['#047378', '#19bd9a'],
      duration:            400,
      wrpClass:            'circles-wrp',
      textClass:           'circles-text',
      valueStrokeClass:    'circles-valueStroke',
      maxValueStrokeClass: 'circles-maxValueStroke',
      styleWrapper:        true,
      styleText:           true
    });
    var circleWebDesign = Circles.create({
      id:                  'circle-web-design',
      radius:              70,
      value:               75,
      maxValue:            100,
      width:               3,
      text:                function(value){return value + '%';},
      colors:              ['#047378', '#19bd9a'],
      duration:            400,
      wrpClass:            'circles-wrp',
      textClass:           'circles-text',
      valueStrokeClass:    'circles-valueStroke',
      maxValueStrokeClass: 'circles-maxValueStroke',
      styleWrapper:        true,
      styleText:           true
    });
    var circleuiux = Circles.create({
      id:                  'circle-ui-ux',
      radius:              70,
      value:               60,
      maxValue:            100,
      width:               3,
      text:                function(value){return value + '%';},
      colors:              ['#047378', '#19bd9a'],
      duration:            400,
      wrpClass:            'circles-wrp',
      textClass:           'circles-text',
      valueStrokeClass:    'circles-valueStroke',
      maxValueStrokeClass: 'circles-maxValueStroke',
      styleWrapper:        true,
      styleText:           true
    });
  }

  if ((scrolled >= 5200) && !isSlide1Inited){
      isSlide1Inited = true;
      initProgressbar('#slide1-progress-bar-branding', 0.8);
      initProgressbar('#slide1-progress-bar-web-design', 0.65);
      initProgressbar('#slide1-progress-bar-ui', 0.75);
  }


}
