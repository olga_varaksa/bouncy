function initProgressbar(id, value){
  new ProgressBar.Line(id, {
    strokeWidth: 4,
    easing: 'easeInOut',
    duration: 1400,
    color: '#19bd9a',
    trailColor: '#b2b3b3',
    trailWidth: 1,
    svgStyle: {width: '100%', height: '100%'},
    text: {
      style: {
        // Text color.
        // Default: same as stroke color (options.color)
        color: '#999',
        position: 'absolute',
        right: '15px',
        top: '-20px',
        padding: 0,
        margin: 0,
        transform: null
      },
      autoStyleContainer: false
    },
    from: {color: '#FFEA82'},
    to: {color: '#ED6A5A'},
    step: (state, bar) => {
      bar.setText(Math.round(bar.value() * 100) + ' %');
    }
  }).animate(value);

}

var isSlide2Inited = false;
var isSlide3Inited = false;

    const dot1 = document.getElementById('dot1');
    const dot2 = document.getElementById('dot2');
    const dot3 = document.getElementById('dot3');

    dot1.addEventListener('click', function(){
      showCard(card1);
      showDots(dot1);
    });
    dot2.addEventListener('click', function(){
      showCard(card2);
      showDots(dot2);
      if (!isSlide2Inited){
        isSlide2Inited = true;
        initProgressbar('#slide2-progress-bar-branding', 0.65);
        initProgressbar('#slide2-progress-bar-web-design', 0.75);
        initProgressbar('#slide2-progress-bar-ui', 0.8);
      }
    });
    dot3.addEventListener('click', function(){
      showCard(card3);
      showDots(dot3);
      if (!isSlide3Inited){
        isSlide3Inited = true;
        initProgressbar('#slide3-progress-bar-branding', 0.8);
        initProgressbar('#slide3-progress-bar-web-design', 0.5);
        initProgressbar('#slide3-progress-bar-ui', 0.75);
      }
    });


    const card1 = document.querySelector('.team__card1');
    const card2 = document.querySelector('.team__card2');
    const card3 = document.querySelector('.team__card3');

    const dots = document.querySelectorAll('.team__slider_dot');

          function showCard(active){
            card1.style.display = 'none';
            card2.style.display = 'none';
            card3.style.display = 'none';

            active.style.display = 'flex';
            }

            function showDots(bg){
                for (var i=0; i<dots.length; i++){
                  dots[i].style.background = 'white';
                }
            bg.style.background = ' #19bd9a';
              }
