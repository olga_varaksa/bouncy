    const tabs0 = document.getElementById('all');
    const tabs1 = document.getElementById('print');
    const tabs2 = document.getElementById('identify');
    const tabs3 = document.getElementById('branding');
    const tabs4 = document.getElementById('web');
    const tabs5 = document.getElementById('html');
    const tabs6 = document.getElementById('wordpress');

    tabs0.addEventListener('click', function(){
      showTab(allBlock);
      showBorder(tabs0);
    });
    tabs1.addEventListener('click', function(){
      showTab(printBlock);
      showBorder(tabs1);
    });
    tabs2.addEventListener('click', function(){
      showTab(identifyBlock);
      showBorder(tabs2);
    });
    tabs3.addEventListener('click', function(){
      showTab(brandingBlock);
      showBorder(tabs3);
    });
    tabs4.addEventListener('click', function(){
      showTab(webBlock);
      showBorder(tabs4);
    });
    tabs5.addEventListener('click', function(){
      showTab(htmlBlock);
        showBorder(tabs5);
    });
    tabs6.addEventListener('click', function(){
      showTab(wordpressBlock);
      showBorder(tabs6);
    });

    const allBlock = document.querySelectorAll('.all');
    const printBlock = document.querySelectorAll('.print');
    const identifyBlock = document.querySelectorAll('.identify');
    const brandingBlock = document.querySelectorAll('.branding');
    const webBlock = document.querySelectorAll('.web');
    const htmlBlock = document.querySelectorAll('.html');
    const wordpressBlock = document.querySelectorAll('.wordpress');

    const tabs = document.querySelectorAll('.tabs');

          function showTab(active){
              for (var i=0; i<allBlock.length; i++){
                allBlock[i].style.display = 'none';
              }
              for (var i=0; i<active.length; i++){
                active[i].style.display = 'block';
              }
            }

            function showBorder(border){
                for (var i=0; i<tabs.length; i++){
                  tabs[i].style.borderBottom = '0px';
                }
              border.style.borderBottom = 'solid 2px #19bd9a';
              }
