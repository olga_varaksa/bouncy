    const dot_1 = document.getElementById('testimonialsdot1');
    const dot_2 = document.getElementById('testimonialsdot2');
    const dot_3 = document.getElementById('testimonialsdot3');

    dot_1.addEventListener('click', function(){
      show_Card(card_1);
      show_Dots(dot_1);
    });
    dot_2.addEventListener('click', function(){
      show_Card(card_2);
      show_Dots(dot_2);
    });
    dot_3.addEventListener('click', function(){
      show_Card(card_3);
      show_Dots(dot_3);
    });


    const card_1 = document.querySelector('.testimonials__card1');
    const card_2 = document.querySelector('.testimonials__card2');
    const card_3 = document.querySelector('.testimonials__card3');

    const dots_ = document.querySelectorAll('.testimonials__slider_dot');

          function show_Card(active){
            card_1.style.display = 'none';
            card_2.style.display = 'none';
            card_3.style.display = 'none';

            active.style.display = 'flex';
            }

            function show_Dots(bg){
                for (var i=0; i<dots_.length; i++){
                  dots_[i].style.background = 'white';
                }
            bg.style.background = ' #19bd9a';
              }
